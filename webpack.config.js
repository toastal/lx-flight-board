"use strict"

const path = require("path")
const webpack = require("webpack")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const TerserPlugin = require("terser-webpack-plugin")


const environment = process.env.NODE_ENV || "development"
const isDev = environment === "development"
const isWebpackDevServer = process.argv.some((a) => path.basename(a) === "webpack-dev-server")
const isWatch = process.argv.some((a) => a === "--watch")
const { broswerslist } = require("./package.json")

// https://github.com/webpack/webpack/issues/7300#issuecomment-413959996
class MiniCssExtractPluginCleanup {
  constructor(deleteWhere = /\.js(\.map)?$/) {
    this.shouldDelete = new RegExp(deleteWhere)
  }
  apply(compiler) {
    compiler.hooks.emit.tapAsync("MiniCssExtractPluginCleanup", (compilation, callback) => {
      Object.keys(compilation.assets).forEach((asset) => {
        if (this.shouldDelete.test(asset)) {
          delete compilation.assets[asset]
        }
      })
      callback()
    })
  }
}

module.exports = {
  target: "web",
  mode: environment,

  entry: {
    main: path.resolve("index.js"),
  },

  output: {
    path: path.resolve("dist", environment),
    filename: `[name]${isDev ? "" : "+[contenthash:20]"}.js`,
  },

  /*
  resolve: {
    modules: [ "node_modules", ".spago" ],
    extensions: [ ".js", ".purs" ],
  },
  */

  plugins: [
    new webpack.DefinePlugin({
      VERSION: JSON.stringify(require("./package.json").version || "UNVERSION"),
    }),
    new HtmlWebpackPlugin({
      target: "web",
      title: "Language Express Flight Board",
      template: path.resolve("index.html"),
    }),
  ].concat(
    isDev ? [] : [
      new MiniCssExtractPlugin({
        filename: `[name]${isDev ? "" : "+[contenthash:20]"}.css`,
      }),
      new MiniCssExtractPluginCleanup(),
    ]
  ),

  optimization: {
    minimizer: [
      new TerserPlugin({
        cache: isDev,
        parallel: true,
        terserOptions: {
          safari10: true,
          keep_fnames: isDev,
          keep_classnames: isDev,
          compress: {
            passes: 2,
            keep_fargs: false,
            keep_infinity: true,
            pure_getters: true,
            // top_retain: topRetain,
            unsafe: true,
            unsafe_proto: true,
            unsafe_comps: true,
          },
          mangle: isDev ? false : {
            // reserved: topRetain,
          },
          output: {
            indent_level: 2,
            comments: isDev,
            beautify: isDev,
          },
        },
      }),
    ],
  },

  module: {
    rules: [
      {
        test: /\.html?$/,
        use: [
          {
            loader: "html-loader",
            options: {
              minimize: true },
          },
        ],
      },
      {
        test: /\.sss$/,
        use: [
          {
            loader: "style-loader",
          },
          {
            loader: "css-loader",
            options: {
              importLoaders: 1,
            },
          },
          {
            loader: "postcss-loader",
            options: {
              ident: "postcss",
              parser: "sugarss",
              plugins: [
                require("postcss-easy-import")({
                  extensions: [ ".css", ".sss" ],
                }),
                require("postcss-normalize")({
                  browsers: broswerslist,
                  forceImport: true,
                }),
                require("postcss-simple-vars"),
                require("postcss-nested")(),
                require("postcss-flexbugs-fixes")(),
                require("postcss-preset-env")({
                  browsers: broswerslist,
                  stage: false,
                  features: {
                    "media-query-ranges": true,
                  },
                  autoprefixer: {
                    flexbox: "no-2009",
                    grid: true,
                  },
                }),
                require("postcss-csso")({
                  restructure: true,
                  forceMediaMerge: true,
                }),
              ],
            },
          },
        ],
      },
      {
        test: /\.purs$/,
        exclude: [ path.resolve("node_modules") ],
        use: [
          {
            loader: "purs-loader",
            options: {
              src: [
                path.resolve(".spago", "*", "*", "src", "**", "*.purs"),
                path.resolve("src", "**", "*.purs"),
              ],
              psc: "psa",
              // pscIde: true,
              pscIdeColors: true,
              watch: isWebpackDevServer || isWatch,
            },
          },
        ],
      },
      {
        test: /\.(json|txt)$/,
        exclude: [ path.resolve("node_modules") ],
        use: [
          {
            loader: "file-loader",
            options: {
              name: `[name]${isDev ? "" : "+[hash:20]"}.[ext]`,
            },
          },
        ],
      },
      {
        test: /\.csv$/,
        exclude: [ path.resolve("node_modules") ],
        use: [
          {
            loader: "file-loader",
          },
        ],
      },
      {
        test: /\.(woff|woff2)$/,
        use: {
          loader: "file-loader",
          options: {
            limit: 20000,
            mimetype: "application/font-woff",
            name: "fonts/[name].[ext]",
          },
        },
      },
    ],
  },
}
