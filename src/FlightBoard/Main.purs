module FlightBoard.Main (main) where

import FlightBoard.Prelude
--
import Effect (Effect)
import Effect.Exception (error)
import Halogen.Aff as HA
import Halogen.VDom.Driver (runUI)
import Web.DOM.ParentNode (QuerySelector(..))
--
import FlightBoard.Board as Board

main ∷ Effect Unit
main = do
  HA.runHalogenAff do
    board ← do
      HA.awaitLoad
      mfb ← HA.selectElement (QuerySelector "#flight-board")
      maybe (throwError (error "`#flight-board` element missing … but how?")) pure mfb
    runUI Board.component unit board
