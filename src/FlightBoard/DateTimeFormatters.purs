module FlightBoard.DateTimeFormatters where

import Data.JSDate (JSDate)

foreign import enCurrentTimeFormat ∷ JSDate → String

foreign import thCurrentTimeFormat ∷ JSDate → String

foreign import enLessonTimeFormat ∷ JSDate → String

foreign import thLessonTimeFormat ∷ JSDate → String
