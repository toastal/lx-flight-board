module FlightBoard.Schedule where

import FlightBoard.Prelude
--
import Data.Array as Array
import Data.DateTime (DateTime)
--
import FlightBoard.Lesson (Lesson, ProjectedLesson, Remark)
import FlightBoard.Lesson as Lesson

type Lessons
  = Array Lesson

type Schedule
  = { lessons ∷ Lessons
    , currentRemark ∷ Maybe Remark -- used to check if we need to update
    , projection ∷ Array ProjectedLesson -- used as a cache for the view
    , toShow ∷ Int
    }

fromLessons ∷ Int → DateTime → Array Lesson → Schedule
fromLessons toShow now alessons = { lessons, projection, currentRemark, toShow }
  where
  lessons = Array.dropWhile (Lesson.isKill now) alessons

  currentRemark = getCurrentRemark now lessons

  projection = makeProjection toShow now lessons

makeProjection ∷ Int → DateTime → Lessons → Array ProjectedLesson
makeProjection toShow now = map (\l → Lesson.projectLesson l (Lesson.remark now l)) ∘ Array.take toShow

getCurrentRemark ∷ DateTime → Lessons → Maybe Remark
getCurrentRemark now = map (Lesson.remark now) ∘ Array.head

hasNewRemark ∷ DateTime → Schedule → Boolean
hasNewRemark now schedule = do
  schedule.currentRemark /= getCurrentRemark now schedule.lessons

maybeIt'sTimeToChange ∷ DateTime → Schedule → Maybe Schedule
maybeIt'sTimeToChange now schedule =
  guard (hasNewRemark now schedule)
    $> do
        { lessons, currentRemark, projection, toShow: schedule.toShow }
  where
  lessons = Array.dropWhile (Lesson.isKill now) schedule.lessons

  currentRemark = getCurrentRemark now lessons

  projection = makeProjection schedule.toShow now lessons
