module FlightBoard.Language where

import FlightBoard.Prelude

data Language
  = English
  | Thai

derive instance eqLanguage ∷ Eq Language

derive instance ordLanguage ∷ Ord Language

allLanguages ∷ Array Language
allLanguages =
  [ English
  , Thai
  ]
