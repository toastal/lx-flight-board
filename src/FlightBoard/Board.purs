module FlightBoard.Board (component) where

import FlightBoard.Prelude
--
import Affjax as AX
import Affjax.ResponseFormat as AXRF
import Control.Monad.Except (withExcept)
import Control.Parallel (parallel, sequential)
import Data.DateTime (DateTime)
import Data.DateTime as DateTime
import Data.Enum (toEnum)
import Data.Time (Time(..))
import Data.Time as Time
import Data.Time.Duration as Duration
import Data.JSDate as JSDate
import Data.TraversableWithIndex (traverseWithIndex)
import Effect.Aff (Aff)
import Effect.Aff.Class (class MonadAff)
import Effect.Now (nowDateTime)
import Effect.Ref as Ref
import Effect.Unsafe (unsafePerformEffect)
import Foreign (ForeignError, readArray)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Elements.Keyed as HK
--import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.Query.EventSource as HES
import Math (abs)
import Network.RemoteData (RemoteData(..))
import Network.RemoteData as RemoteData
import Partial.Unsafe (unsafePartial)
import Web.HTML as HTML
import Web.HTML.Window as Window
import Web.HTML.Location as Location
--
import PapaParse as PapaParse
import FlightBoard.DateTimeFormatters (enCurrentTimeFormat)
import FlightBoard.Donut as Donut
import FlightBoard.Lesson (ProjectedLesson)
import FlightBoard.Lesson as Lesson
import FlightBoard.Schedule (Schedule)
import FlightBoard.Schedule as Schedule

lessonsToShow ∷ Int
lessonsToShow = 15

timeToReload ∷ Time
timeToReload =
  unsafePartial $ fromJust
    $ Time
    <$> toEnum (bangkokifyHour 7)
    <*> toEnum 30
    <*> toEnum 0
    <*> toEnum 0
  where
  bangkokifyHour ∷ Int → Int
  bangkokifyHour i = mod (i - 7) 24

type State
  = { now ∷ DateTime
    , schedule ∷ RemoteData Error Schedule
    , subscription ∷ Maybe H.SubscriptionId
    }

data Error
  = ResponseErr AX.Error
  | ForeignErr (NonEmptyList ForeignError)

data Action
  = Initialize
  | Finalize
  | Tick

component ∷ ∀ q i o m. MonadAff m ⇒ H.Component HH.HTML q i o m
component =
  H.mkComponent
    { initialState
    , render
    , eval:
        H.mkEval
          $ H.defaultEval
              { handleAction = handleAction
              , initialize = Just Initialize
              }
    }

initialState ∷ ∀ i. i → State
initialState _ =
  { now: unsafePerformEffect nowDateTime -- This doesn't fail & I'm lazy
  , schedule: Loading
  , subscription: Nothing
  }

render ∷ ∀ m. State → H.ComponentHTML Action () m
render state =
  HH.div_
    [ renderMast state.now
    , case state.schedule of
        Loading →
          HH.div
            [ HP.class_ $ HH.ClassName "PageLoader" ]
            [ Donut.spinner ]
        Success s → renderSchedule s
        Failure e →
          HH.div
            [ HP.class_ $ HH.ClassName "PageLoader" ]
            [ HH.text "Failed to load and parse schedule CSV" ]
        NotAsked →
          HH.div
            [ HP.class_ $ HH.ClassName "PageLoader" ]
            [ HH.text "This state shouldn't exist" ]
    ]

renderMast ∷ ∀ p i. DateTime → HH.HTML p i
renderMast now =
  HH.header
    [ HP.class_ $ HH.ClassName "Mast" ]
    [ HH.div
        [ HP.class_ $ HH.ClassName "Mast-main" ]
        [ HH.img
            [ HP.class_ $ HH.ClassName "Mast-logo"
            , HP.src "./lx-logo.svg"
            ]
        , HH.h1
            [ HP.class_ $ HH.ClassName "Mast-title" ]
            [ HH.text "Language Express Flight Board" ]
        ]
    , HH.time
        [ HP.class_ $ HH.ClassName "Mast-aux Mast-time" ]
        [ HH.text $ enCurrentTimeFormat (JSDate.fromDateTime now) ]
    ]

renderSchedule ∷ ∀ p i. Schedule → HH.HTML p i
renderSchedule shedule =
  HH.table
    [ HP.class_ (HH.ClassName "Schedule") ]
    [ HH.thead
        [ HP.class_ (HH.ClassName "Schedule-head") ]
        [ HH.th [ HP.class_ shI ]
            [ HH.text "Time" ]
        , HH.th [ HP.class_ shI ]
            [ HH.abbr [ HP.title "Language" ] [ HH.text "Lang." ] ]
        , HH.th [ HP.class_ shI ]
            [ HH.text "Level" ]
        , HH.th [ HP.class_ shI ]
            [ HH.text "Title" ]
        , HH.th [ HP.class_ shI ]
            [ HH.text "Book" ]
        , HH.th [ HP.class_ shI ]
            [ HH.text "Room" ]
        , HH.th [ HP.class_ shI ]
            [ HH.text "Remark" ]
        ]
    , HK.tbody [] (map renderLesson shedule.projection)
    ]
  where
  shI = HH.ClassName "Schedule-headItem"

renderLesson ∷ ∀ p i. ProjectedLesson → Tuple String (HH.HTML p i)
renderLesson lesson =
  Tuple lesson.id
    $ HH.tr
        [ HP.class_ $ HH.ClassName "Lesson" ]
        [ HH.td
            [ HP.classes $ lessonField "time" ]
            [ HH.text lesson.startTime ]
        , HH.td
            [ HP.classes $ lessonField "langCode" ]
            [ HH.text lesson.langCode ]
        , HH.td
            [ HP.classes $ lessonFieldPlus "level" lesson.level ]
            [ HH.text lesson.level ]
        , HH.td
            [ HP.classes $ lessonField "title" ]
            [ HH.text lesson.title ]
        , HH.td
            [ HP.classes $ lessonField "book" ]
            [ if lesson.book == "Conversation" then
                HH.abbr [ HP.title "Conversation" ] [ HH.text "Conv." ]
              else
                HH.text lesson.book
            ]
        , HH.td
            [ HP.classes $ lessonField "room" ]
            [ HH.text lesson.room ]
        , HH.td
            [ HP.classes $ lessonFieldPlus "remark" lesson.remark.show ]
            [ HH.text lesson.remark.label ]
        ]
  where
  lessonField t = HH.ClassName <$> [ "Lesson-field", "Lesson-field--" <> t ]

  lessonFieldPlus t u =
    HH.ClassName
      <$> [ "Lesson-field", "Lesson-field--" <> t, "Lesson-field--" <> u ]

handleAction ∷ ∀ o m. MonadAff m ⇒ Action → H.HalogenM State Action () o m Unit
handleAction = case _ of
  Tick → do
    now ← H.liftEffect nowDateTime
    when (shouldReloadPage now)
      $ H.liftEffect do
          loc ← Window.location =<< HTML.window
          Location.reload loc
    H.gets _.schedule
      >>= traverse_ \schedule → case Schedule.maybeIt'sTimeToChange now schedule of
          Nothing → H.modify (_ { now = now })
          Just newSchedule → H.modify (_ { now = now, schedule = Success newSchedule })
  Initialize → do
    handleAction Finalize
    subscription ←
      H.subscribe
        $ HES.effectEventSource \emitter → do
            ref ← Ref.new Nothing
            let
              loop = do
                HES.emit emitter Tick
                id ← Window.requestAnimationFrame loop =<< HTML.window
                Ref.write (Just id) ref
            loop
            pure
              $ HES.Finalizer do
                  Ref.read ref
                    >>= traverse_ \id →
                        HTML.window >>= Window.cancelAnimationFrame id
    let
      loadParse csv = RemoteData.fromEither <$> parallel (parseRequestedCSV csv)
    schedule ← H.liftAff $ sequential $ loadParse "./schedule.csv"
    now ← H.liftEffect nowDateTime
    H.modify_ (_ { now = now, schedule = schedule, subscription = pure subscription })
  Finalize →
    H.gets _.subscription
      >>= traverse_ \subscription → do
          H.unsubscribe subscription
          H.modify (_ { subscription = Nothing })

parseRequestedCSV ∷ String → Aff (Either Error Schedule)
parseRequestedCSV url = do
  response ← bimap ResponseErr _.body <$> AX.get AXRF.string url
  csv ← traverse PapaParse.parse response
  case csv of
    Left r → pure (Left r)
    Right c → do
      now ← H.liftEffect nowDateTime
      let
        readSchedule = map (Schedule.fromLessons lessonsToShow now) ∘ traverseWithIndex Lesson.readLesson <=< readArray
      pure (runExcept ∘ withExcept ForeignErr $ readSchedule c)

shouldReloadPage ∷ DateTime → Boolean
shouldReloadPage now = abs (unwrap delta) < 0.8
  where
  delta ∷ Duration.Seconds
  delta =
    Duration.convertDuration
      $ (Time.diff timeToReload (DateTime.time now) ∷ Duration.Milliseconds)
