/* FlightBoard.DateTimeFormaters */

// kinda hacky to put this all here
// but it doesn’t make the app better to have this in my state

var enLocale = "en-GB-u-ca-iso8601";
var thLocale = "th-TH";
var tz = "Asia/Bangkok";

var currentTimeOpts = {
  timeZone: tz,
  year: "numeric",
  month: "short",
  day: "numeric",
  weekday: "short",
  hour: "numeric",
  hour12: false,
  minute: "2-digit"
};

var lessonTimeOpts = {
  timeZone: tz,
  weekday: "narrow",
  hour: "2-digit",
  hour12: false,
  minute: "2-digit"
};

var enCurrentTime = new Intl.DateTimeFormat(enLocale, currentTimeOpts);
var thCurrentTime = new Intl.DateTimeFormat(thLocale, currentTimeOpts);

var enLessonTime = new Intl.DateTimeFormat(enLocale, lessonTimeOpts);
var thLessonTime = new Intl.DateTimeFormat(thLocale, lessonTimeOpts);

exports.enCurrentTimeFormat = function(date) {
  return enCurrentTime.format(date);
};

exports.thCurrentTimeFormat = function(date) {
  return thCurrentTime.format(date);
};

exports.enLessonTimeFormat = function(date) {
  return enLessonTime.format(date);
};

exports.thLessonTimeFormat = function(date) {
  return thLessonTime.format(date);
};
