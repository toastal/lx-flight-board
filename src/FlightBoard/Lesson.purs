module FlightBoard.Lesson
  ( Lesson
  , readLesson
  , ProjectedLesson
  , projectLesson
  , Remark(..)
  , remarkToString
  , remark
  , isKill
  ) where

import FlightBoard.Prelude
--
import Control.Monad.Except (mapExcept)
import Data.Array as Array
import Data.DateTime (DateTime)
import Data.DateTime as DateTime
import Data.Either as Either
import Data.JSDate as JSDate
import Data.List.NonEmpty as NEL
import Data.String (trim)
import Data.String.Regex (Regex)
import Data.String.Regex as Regex
import Data.String.Regex.Flags as RegexFlags
import Data.Time.Duration (Milliseconds, Minutes)
import Data.Time.Duration as Duration
import Effect.Unsafe (unsafePerformEffect)
import Foreign (F, Foreign, ForeignError(..), readString, tagOf)
import Foreign.Index ((!))
import Partial.Unsafe (unsafePartial)
--
import FlightBoard.DateTimeFormatters (enLessonTimeFormat)

type Lesson
  = { id ∷ String
    , startTime ∷ DateTime
    , langCode ∷ String
    , level ∷ String
    , title ∷ String
    , book ∷ String
    , room ∷ String
    }

readLesson ∷ Int → Foreign → F Lesson
readLesson id_ v = ado
  startTime ← readDate =<< v ! "dateTime"
  langCode ← readString =<< v ! "language"
  level ← readString =<< v ! "level"
  title ← readString =<< v ! "title"
  book ← readString =<< v ! "book"
  room ← readString =<< v ! "room"
  in { id: langCode <> show id_, startTime, level, langCode, title, book, room }
  where
  readDate ∷ Foreign → F DateTime
  readDate value = mapExcept (either (const error) (fromJSDate ∘ dateNormalizing)) (readString value)
    where
    dateNormalizing ∷ String → String
    dateNormalizing = addBKKTimezoneIfNeeded ∘ tTagger ∘ fixWrongDateFormat ∘ trim

    -- T needed be between ${date}T${time} for Webkit
    tTagger ∷ String → String
    tTagger = Regex.replace tTaggerRegex "$1T$2"

    -- Sometimes our schedule CSV uploader doesn’t understand what an ISO-formatted date is :|
    fixWrongDateFormat ∷ String → String
    fixWrongDateFormat = Regex.replace' wrongDateRegex wrongDateTransformer

    addBKKTimezoneIfNeeded ∷ String → String
    addBKKTimezoneIfNeeded s
      | Regex.test tzRegex s = s
      -- doesn’t fix errors in the Regex, just appends the Bangkok offset if no tz present
      | otherwise = s <> "+07:00"

    fromJSDate ∷ String → Either (NonEmptyList ForeignError) DateTime
    fromJSDate = maybe error pure ∘ JSDate.toDateTime ∘ unsafePerformEffect ∘ JSDate.parse

    error ∷ ∀ a. Either (NonEmptyList ForeignError) a
    error = Left $ NEL.singleton $ TypeMismatch "DateTime" (tagOf value)

  wrongDateTransformer ∷ String → Array String → String
  wrongDateTransformer _ bits =
    fromMaybe "" ado
      day ← Array.index bits 0
      month ← Array.index bits 1
      year ← Array.index bits 2
      time ← Array.index bits 3
      in year <> "-" <> monthToDigit month <> "-" <> day <> "T" <> time

  monthToDigit ∷ String → String
  monthToDigit = case _ of
    "Jan" → "01"
    "Feb" → "02"
    "Mar" → "03"
    "Apr" → "04"
    "May" → "05"
    "Jun" → "06"
    "Jul" → "07"
    "Aug" → "08"
    "Sep" → "09"
    "Oct" → "10"
    "Nov" → "11"
    "Dec" → "12"
    _ → "01"

tTaggerRegex ∷ Regex
tTaggerRegex =
  unsafePartial Either.fromRight
    $ Regex.regex "^([0-9]{4}-[0-1][0-9]-[0-3][0-9]) (\\d.*)" RegexFlags.noFlags

wrongDateRegex ∷ Regex
wrongDateRegex =
  unsafePartial Either.fromRight
    $ Regex.regex "^([0-3][0-9])-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-([0-9]{4}) (\\d.*)" RegexFlags.noFlags

tzRegex ∷ Regex
tzRegex =
  unsafePartial Either.fromRight
    $ Regex.regex "[+-][0-2][0-9]:[0-5][0-9]$" RegexFlags.noFlags

type ProjectedLesson
  = { id ∷ String
    , startTime ∷ String
    , langCode ∷ String
    , level ∷ String
    , title ∷ String
    , book ∷ String
    , room ∷ String
    , remark ∷ { label ∷ String, show ∷ String }
    }

projectLesson ∷ Lesson → Remark → ProjectedLesson
projectLesson lesson remark_ =
  { id: lesson.id
  , startTime: enLessonTimeFormat (JSDate.fromDateTime lesson.startTime)
  , langCode: lesson.langCode
  , level: lesson.level
  , title: lesson.title
  , book: lesson.book
  , room: lesson.room
  , remark: { label: remarkToString remark_, show: show remark_ }
  }

data Remark
  = OnTime
  | Boarding
  | LastCall
  | Departed
  | Arrived
  | Kill

derive instance eqRemark ∷ Eq Remark

derive instance ordRemark ∷ Ord Remark

instance showRemark ∷ Show Remark where
  show = case _ of
    OnTime → "OnTime"
    Boarding → "Boarding"
    LastCall → "LastCall"
    Departed → "Departed"
    Arrived → "Arrived"
    Kill → "Kill"

remarkToString ∷ Remark → String
remarkToString = case _ of
  OnTime → "On Time"
  Boarding → "Boarding"
  LastCall → "Last Call"
  Departed → "Departed"
  Arrived → "Arrived"
  Kill → ""

remark ∷ ∀ r. DateTime → { startTime ∷ DateTime | r } → Remark
remark now { startTime } = remark' (unwrap delta)
  where
  delta ∷ Minutes
  delta = Duration.convertDuration (DateTime.diff startTime now ∷ Milliseconds)

  remark' ∷ Number → Remark
  remark' d
    | d <= -55.0 = Kill
    | d <= -50.0 = Arrived
    | d <= -10.0 = Departed
    | d <= 0.0 = LastCall
    | d <= 10.0 = Boarding
    | otherwise = OnTime

isKill ∷ ∀ r. DateTime → { startTime ∷ DateTime | r } → Boolean
isKill now = (_ == Kill) ∘ remark now
