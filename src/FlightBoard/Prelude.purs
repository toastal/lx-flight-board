module FlightBoard.Prelude
  ( (∘)
  , (≡)
  , (≠)
  , (×)
  , (∨)
  , (∧)
  , (⨁)
  , (⊹)
  , type (⨁)
  , type (⊹)
  , type (×)
  , module Prelude
  , module Control.Alt
  , module Control.Apply
  , module Control.Bind
  , module Control.Monad
  , module Control.Monad.Error.Class
  , module Control.Monad.Except
  , module Control.Monad.Gen.Class
  , module Control.Monad.Maybe.Trans
  , module Control.Monad.Reader
  , module Control.Monad.Rec.Class
  , module Control.Monad.Trans.Class
  , module Control.MonadPlus
  , module Control.Parallel
  , module Control.Plus
  , module Data.Bifoldable
  , module Data.Bifunctor
  , module Data.Bitraversable
  , module Data.Const
  , module Data.Either
  , module Data.Foldable
  , module Data.Functor
  , module Data.Functor.Coproduct
  , module Data.Generic.Rep
  , module Data.Generic.Rep.Show
  , module Data.List
  , module Data.List.NonEmpty
  , module Data.Maybe
  , module Data.Monoid
  , module Data.Newtype
  , module Data.Set
  , module Data.Symbol
  , module Data.Traversable
  , module Data.Tuple
  , module Data.Variant
  , module Data.Void
  ) where

import Prelude
--
import Control.Alt (class Alt, (<|>))
import Control.Apply ((*>), (<*))
import Control.Bind (join, (>=>), (<=<))
import Control.Monad (when, unless)
import Control.Monad.Error.Class (class MonadError, class MonadThrow, throwError, catchError)
import Control.Monad.Except (ExceptT(..), runExcept, runExceptT, except)
import Control.Monad.Gen.Class (class MonadGen)
import Control.Monad.Maybe.Trans (MaybeT(..), runMaybeT)
import Control.Monad.Reader (class MonadAsk, class MonadReader, ask, asks)
import Control.Monad.Rec.Class (class MonadRec)
import Control.Monad.Trans.Class (class MonadTrans, lift)
import Control.MonadPlus (class MonadPlus, guard)
import Control.Parallel (class Parallel, parTraverse, parTraverse_)
import Control.Plus (class Plus, empty)
import Data.Bifoldable (class Bifoldable, bitraverse_, bifor_)
import Data.Bifunctor (class Bifunctor, bimap, lmap, rmap)
import Data.Bitraversable (class Bitraversable, bitraverse, bisequence, bifor)
import Data.Const (Const(..))
import Data.Either (Either(..), either, isLeft, isRight, fromRight, note, hush)
import Data.Foldable (class Foldable, traverse_, for_, foldMap, foldl, foldr, fold)
import Data.Functor (($>), (<$))
import Data.Functor.Coproduct (Coproduct, coproduct, left, right)
import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Show (genericShow)
import Data.List (List)
import Data.List.NonEmpty (NonEmptyList)
import Data.Maybe (Maybe(..), fromMaybe, fromMaybe', isJust, isNothing, maybe, maybe', fromJust)
import Data.Monoid (class Monoid, mempty)
import Data.Newtype (class Newtype, unwrap, ala, alaF, un)
import Data.Set (Set)
import Data.Symbol (class IsSymbol)
import Data.Traversable (class Traversable, traverse, sequence, for)
import Data.Tuple (Tuple(..), fst, snd, uncurry)
import Data.Variant (SProxy(..), Variant, case_, class Contractable, contract, expand)
import Data.Void (Void, absurd)

infixr 9 compose as ∘

infixr 1 Tuple as ×

infix 4 eq as ≡

infix 4 notEq as ≠

infixr 3 conj as ∧

infixr 2 disj as ∨

infixr 4 type Either as ⊹

infixr 4 type Coproduct as ⨁

infixr 5 either as ⊹

infixr 5 coproduct as ⨁

infixr 4 type Tuple as ×
