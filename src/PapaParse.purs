module PapaParse (parse) where

import Prelude
--
import Effect.Aff (Aff)
import Effect.Aff.Compat (EffectFnAff, fromEffectFnAff)
import Foreign (Foreign)

foreign import _parse ∷ String → EffectFnAff Foreign

parse ∷ String → Aff Foreign
parse = fromEffectFnAff <<< _parse
