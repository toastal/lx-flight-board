/* PapaParse */

var Papa = require("papaparse");
var camelCase = require("camelcase");

var conf = {
  delimitersToGuess: [ ",", ";" ],
  quoteChar: "\"",
  fastMode: false,
  skipEmptyLines: "greedy",
  header: true,
  transformHeader: function(h) {
    return camelCase(h.trim(), { pascalCase: false });
  }
};

exports._parse = function (string) {
  return function (onError, onSuccess) {
    conf.complete = function(result) {
      var errors = result.errors;
      if (errors.length > 0) {
        var err = new Error(JSON.stringify(errors, null, 1));
        err.name = "PapaParseError" + errors.length < 2 ? "" : "s";
        onError(err);
      } else {
        onSuccess(result.data);
      }
    };

    Papa.parse(string, conf);

    return function (cancelError, cancelerError, cancelerSuccess) {
      // there’s not a way to do this???
      delete conf.complete;
      cancelerSuccess();
    };
  };
};
