module.exports = {
  plugins: [
    require("postcss-easy-import")({
      extensions: [ ".css", ".sss" ],
    }),
    require("postcss-normalize")({
      forceImport: true,
    }),
    require("postcss-simple-vars"),
    require("postcss-nested")({}),
    require("postcss-flexbugs-fixes")(),
    require("postcss-preset-env")({
      stage: false,
      features: {
        "media-query-ranges": true,
      },
      autoprefixer: {
        flexbox: "no-2009",
        grid: true,
      },
    }),
    // require("postcss-csso")({
    //  restructure: true,
    //  forceMediaMerge: true,
    // }),
  ],
}
