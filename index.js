import { version } from "./package.json"

function main() {
  if (process.env.NODE_ENV === "production") {
    require("./output/flight-board.js")()
  } else {
    require("./output/FlightBoard.Main").main()
  }
}

const v = (function(v) {
  return v == null || v === "" || v === "null" || v === "undefined"
    ? "" : "v" + v
})(version)
console.log("🛫 LX Flight Board", v || "UNVERSIONED")

if (module.hot) {
  module.hot.accept(function() {
    Array.prototype.forEach.call(
      document.querySelectorAll("#flight-board>div"),
      function(d) { d.remove() },
    )
    main()
  })
}

main()
