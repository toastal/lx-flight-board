{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ sources =
    [ "src/**/*.purs", "test/**/*.purs" ]
, name =
    "flight-board"
, dependencies =
    [ "affjax"
    , "assert"
    , "console"
    , "datetime"
    , "debug"
    , "effect"
    , "either"
    , "functions"
    , "generics-rep"
    , "halogen"
    , "js-date"
    , "maybe"
    , "nonempty"
    , "now"
    , "nullable"
    , "parallel"
    , "partial"
    , "prelude"
    , "record"
    , "remotedata"
    , "tuples"
    , "variant"
    , "web-dom"
    ]
, packages =
    ./packages.dhall
}
