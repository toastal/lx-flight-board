# LX Flight Board

## Suggested Tools

- [`asdf`](https://asdf-vm.com/#/): asdf-vm version manager (although, you _can_ use an LTS version matching the `package.json`, this makes that easier)
- [`yarn`](https://yarnpkg.com/lang/en/): JavaScript package manager (`npm` is sufficient but the author is using `yarn` on other projects)


## Setup

```sh
asdf install
yarn install --prefer-offline
```


## Run dev server

```sh
yarn start
```

## Build for production in `dist/production`

```sh
yarn build:prod
```
