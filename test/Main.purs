module Test.Main where

import FlightBoard.Prelude

import Data.Date as Date
import Data.DateTime as DateTime
import Data.Enum (toEnum)
import Data.Time as Time
import Effect (Effect)
import Effect.Console (log)
import Partial.Unsafe (unsafePartial)
import Test.Assert (assert, assertFalse)

import FlightBoard.Lesson as Lesson
import FlightBoard.Schedule as Schedule

main ∷ Effect Unit
main = do
  let
    toTime ∷ Int → Int → Int → Int → Time.Time
    toTime h m s ms =
      unsafePartial $ fromJust $ Time.Time <$> toEnum h <*> toEnum m <*> toEnum s <*> toEnum ms

    toDate ∷ Time.Time → DateTime.DateTime
    toDate = DateTime.DateTime
      (unsafePartial $ fromJust $ Date.canonicalDate <$> toEnum 2019 <*> pure Date.May <*> toEnum 14)

    startTime = toDate $ toTime 13 0 0 0
    boardingTime = toDate $ toTime 12 50 0 0
    lastCall = toDate $ toTime 13 1 0 0
    killTime = toDate $ toTime 13 55 0 0

    mkLesson ∷ DateTime.DateTime → Int  → Lesson.Lesson
    mkLesson startTime' id' =
      { id: show id', startTime: startTime', level: "", langCode: "", title: "", book: "", room: "" }

    lesson1 = mkLesson startTime 1
    lesson2 = mkLesson (toDate $ toTime 14 0 0 0) 2

    timeSchedule now = Schedule.fromLessons 2 now [ lesson1, lesson2 ]

  log "∘ remarks"
  assert $ Lesson.isKill killTime lesson1
  assertFalse $ Lesson.isKill boardingTime lesson1
  assert $ Lesson.Boarding == Lesson.remark boardingTime lesson1
  assert $ Lesson.LastCall == Lesson.remark lastCall lesson1

  log "∘ fromLessons"
  assert $ [] == _.lessons (Schedule.fromLessons 1 lastCall [])
  assert $ [] == _.lessons (Schedule.fromLessons 1 killTime [ lesson1 ])
  assert $ [ lesson1 ] == _.lessons (Schedule.fromLessons 1 boardingTime [ lesson1 ])
  assert $ [ lesson2 ] == _.lessons (timeSchedule killTime)
  assert $ [ lesson1, lesson2 ] == _.lessons (timeSchedule boardingTime)

  log "∘ hasNewRemark"
  assertFalse $ Schedule.hasNewRemark startTime (timeSchedule startTime)
  assertFalse $ Schedule.hasNewRemark lastCall (timeSchedule startTime)
  assert $ Schedule.hasNewRemark boardingTime (timeSchedule startTime)
  assert $ Schedule.hasNewRemark killTime (timeSchedule startTime)

  log "∘ maybeIt'sTimeToChange"
  assert $ Nothing == (_.lessons <$> Schedule.maybeIt'sTimeToChange startTime (timeSchedule startTime))
  assert $ Just [ lesson1, lesson2 ] == (_.lessons <$> Schedule.maybeIt'sTimeToChange boardingTime (timeSchedule startTime))
  assert $ Just [ lesson2 ] == (_.lessons <$> Schedule.maybeIt'sTimeToChange killTime (timeSchedule startTime))
